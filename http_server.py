from flask import Flask, request

app = Flask(__name__)

# Define the username and password for authentication
USERNAME = "abcd"
PASSWORD = "1234"


@app.route("/token")
def token():
    auth = request.authorization
    if auth and auth.username == USERNAME and auth.password == PASSWORD:
        return "", 200
    return "", 401


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
