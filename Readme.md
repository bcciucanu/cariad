We can test the server locally
> instructions.md, http_server.py

If we want to scale it up we can use
> instructions_for_image.md, app.py, requirements.txt, Dockerfile

In order to deploy the server in a Kubernetes cluster we need to use

>kubectl apply -f http-server-deployment.yaml

>kubectl apply -f http-server-service.yaml

These manifests will create a Kubernetes Deployment with a single replica of your HTTP server and a Service to expose it.

The Service is of type LoadBalancer (which is suitable for deployments in cloud environments that support load balancers)

If we are deploying to a local development cluster, we can change the type field to NodePort or ClusterIP depending on our requirements.
