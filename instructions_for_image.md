We create a new directory for the project so we can navigate
>mkdir http-server
>cd http-server

Then we create a new file called "app.py"
> touch app.py
>> attached the file

We can create a basic flask server and handles for the "/token" endpoint with basic auth

we create the Dockerfile 
> touch Dockerfile
>> attached the file

We create a "requirements.txt" file and we add the folowing:
> Flask==2.0.2

We build the Docker Image by running: 
> docker build -t http-server .

Once the image has been built we can run a container by running:
> docker run -d -p 9000:9000 http-server

The container should be running with our Flask server and we can access it on:
> http://localhost:9000/token

To authenticate using Basic Authentication, we'll need to provide a client ID and a client secret in the request headers

We can test it by using the following:
>curl -u client_id:client_secret http://localhost:9000/token

If the credentials are valid, you'll receive a response with HTTP 200 OK. Otherwise, you'll get a 401 Unauthorized response

Remember to replace 'client_id' and 'client_secret' with the actual client credentials you want to authenticate

> username == "abcd" and password == "1234"  will give us the 200 OK code
> using any other combination should give us a 401 response