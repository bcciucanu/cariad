We can use the following command:
python3 http_server.py
In order to run the python script and start the HTTP server

For the script to be able to run we will need flask installed 
>we can install flask with the following command:
pip install flask

Once the script is running it'll keep server active 

We can open a bash terminal and use the following command:
curl --user abcd:1234 http://localhost:8080/token

Which should result with a ""GET /token HTTP/1.1" 200 -" reply in the terminal that is running the HTTP server

>In case we add different username/password combination the reply we'll be receiving would be ""GET /token HTTP/1.1" 401 -"