from flask import Flask, request
from functools import wraps

app = Flask(__name__)


def require_basic_auth(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_credentials(auth.username, auth.password):
            return "Unauthorized", 401
        return func(*args, **kwargs)

    return decorated


def check_credentials(username, password):
    # Check if the provided credentials are valid (implement your own credentials here)
    if username == "abcd" and password == "1234":
        return True
    return False


@app.route("/token")
@require_basic_auth
def token_endpoint():
    return "HTTP 200 OK"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9000)
